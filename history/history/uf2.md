## 10 de Nov 2020

Amb la comanda **lsblk** dona informació sobre les particions com l'espai, etc:

**lsblk:** 
```
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 465.8G  0 disk 
├─sda1   8:1    0     1K  0 part 
├─sda5   8:5    0   100G  0 part /
├─sda6   8:6    0   100G  0 part 
└─sda7   8:7    0     5G  0 part [SWAP]

```

Alias para lsblk amb diferents opcions: 

**lsdisk:**
```
NAME     SIZE MOUNTPOINT MODEL
sda    465.8G            ST500DM002-1BD142
├─sda1     1K            
├─sda5   100G /          
├─sda6   100G            
└─sda7     5G [SWAP]  

```

Amb la comanda df -h serveix per veure el espai lliure del tots el discos:

**df -h:**
```
Filesystem      Size  Used Avail Use% Mounted on
devtmpfs        3.9G     0  3.9G   0% /dev
tmpfs           3.9G  119M  3.8G   4% /dev/shm
tmpfs           3.9G  1.5M  3.9G   1% /run
/dev/sda5        98G   11G   83G  12% /
tmpfs           3.9G   32K  3.9G   1% /tmp
tmpfs           785M   40K  785M   1% /run/user/1000

```


Alias per crear una comanda que mostri columnes interessants d'un disk que m'ofereix la comanda lsblk:
alias lsdisk='lsblk -o NAME,MODEL,FSTYPE,SIZE,TYPE,MOUNTPOINT'
```
NAME     SIZE MOUNTPOINT MODEL
sda    465.8G            ST500DM002-1BD142
├─sda1     1K            
├─sda5   100G /          
├─sda6   100G            
└─sda7     5G [SWAP]

```

dmidecode serveix per extreure informació de la placa base, etc:
```
dmidecode -t baseboard-product-name
dmidecode -s baseboard-product-name
dmidecode -s baseboard-manufacturer
dmidecode -t baseboard
lscpu
lspci -nn

```
Amb lshw s'obte informació molt detallada sobre un node:
```
lshw -class memory
```

dmidecode -s system-product-name
dmidecode -s system-product-name
dmidecode -s mother-board
dmidecode -s baseboard-product-name

dmidecode -t system --version
dmidecode -s baseboard-product-name
dmidecode --help
dmidecode -t --help
dmidecode -s --help

dmidecode -t baseboard
dmidecode --type memory 
dmidecode -t memory


tmux a
dmidecode -t
dmidecode -s
dmidecode -t memory
dmidecode -t 16

lspci -nn |grep 0280
lspci -nn |grep 0200


lspci -nn -v -mm 
lspci -nn -v -mm |grep Ethernet 
lspci -nn -v -mm |grep "Ethernet controller"
lspci -nn -v -mm |grep -B 1 "Ethernet controller"
lspci -nn -v -mm |grep -B 1 -A 2 "Ethernet controller"
lspci -nn -v -mm |grep -B 1 -A 8 "Ethernet controller"

lspci -nn -v -mm -k 
ip a
ifconfig
ifconfig |less

lsblk -O |less

lsblk -o MODEL,NAME,SIZE,MOUNTPOINT

smartctl --help

smartctl --all /dev/sdb |less

lshw -class disk -sanitize

dmidecode -s baseboard-product-name


24 / nov

Instrucciones para abrir puerto y permitir acceso a un systemrescuecd por ssh
## definir la política de aceptar todo lo que entra y sale por la red
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
## Flush y borrado de las reglas
iptables -F
iptables -X
# poner un password a root
passwd
# cambiar la distribución del teclado
setkmap es
Con el Rescue cd podéis:
#Arrancar interface gráfica 
startx

#Compartir terminal con otros
# abrir terminal compartida
tmux

# entrar en una terminal compartida (attach session)
tmux a

Parted
parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 1% 50%
parted -s /dev/vda mkpart primary 50% 60%
parted -s /dev/vda mkpart primary 60% 70%
parted -s /dev/vda mkpart extended 70% 100%
parted -s /dev/vda mkpart logical 70% 75%




24 / nov

Instrucciones para abrir puerto y permitir acceso a un systemrescuecd por ssh
## definir la política de aceptar todo lo que entra y sale por la red
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
## Flush y borrado de las reglas
iptables -F
iptables -X
# poner un password a root
passwd
# cambiar la distribución del teclado
setkmap es
Con el Rescue cd podéis:
#Arrancar interface gráfica 
startx

#Compartir terminal con otros
# abrir terminal compartida
tmux

# entrar en una terminal compartida (attach session)
tmux a

Parted
parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 1% 50%
parted -s /dev/vda mkpart primary 50% 60%
parted -s /dev/vda mkpart primary 60% 70%
parted -s /dev/vda mkpart extended 70% 100%
parted -s /dev/vda mkpart logical 70% 75%




























Prova: dd if=/dev/urandom of=/mnt/prova2G bs=2048 Count=1048576
